-- Database schema for regroup API v0

DROP TABLE IF EXISTS Groups;
CREATE TABLE Groups (
    id BINARY(16) NOT NULL, -- UUID
    name VARCHAR(255) NOT NULL,
    expiry DATETIME NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS Members;
CREATE TABLE Members (
    -- Primary key
    id BINARY(16) NOT NULL, -- UUID
    group_id BINARY(16) NOT NULL, -- UUID
    
    -- Personal information
    client_id VARCHAR(255),
    full_name VARCHAR(255),
    phone VARCHAR(32), -- Stored as packed digits ("16109557279")
    
    -- Location data
    latitude DECIMAL(18, 12),
    longitude DECIMAL(18, 12),
    error_radius DECIMAL(4, 1), -- In meters

    PRIMARY KEY (group_id, id),
    UNIQUE (group_id, id),
    FOREIGN KEY (group_id) REFERENCES Groups(id)
);
