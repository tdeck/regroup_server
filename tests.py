import os
import regroup
import unittest
import tempfile
import json

class RegroupTestCase(unittest.TestCase):
    def setUp(self):
        #regroup.app.config['DATABASE'] = tempfile.mkstemp()
        regroup.app.config['TESTING'] = True 
        self.app = regroup.app.test_client()
        #regroup.init_db()

    def tearDown(self):
        #os.close(self.dbh)
        #os.unlink(regroup.app.config['DATABASE'])
	pass

    def test_create_group(self):
        # Create the group
        response = self.app.post(
            '/api/v0/groups/',
            content_type='application/json',
            data=json.dumps({'name': 'Test group', 'expiry': '2013-04-07T12:52'})
        )
        assert response.status_code == 200
        data = json.loads(response.data)
        assert 'href' in data
        group_href = data['href']

        # Add a first member
        response = self.app.post(        
            group_href + "/members/",
            content_type='application/json',
            data=json.dumps({
                'client_id': 'testbot42',
                'full_name': 'Test Robot',
                'phone': '2156105555',
                'latitude': 100,
                'longitude': 200,
                'error_radius': 3,
            })
        )
        assert response.status_code == 200
        data = json.loads(response.data)
        assert 'href' in data
        member_href = data['href']


if __name__ == '__main__':
    unittest.main()
