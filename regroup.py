from __future__ import with_statement
from flask import Flask, request, jsonify, g, url_for, render_template, abort, redirect
from uuid import UUID
from random import choice, uniform   # Used only for web UI
from dbapi import *
app = Flask(__name__)

DATABASE = "/regroup_server/db.sqlite"
DEBUG = True
WEBVIEW = True
app.config.from_object(__name__)

### Setup functinos
def init_db():
    with closing(connect_to_database(app.config['DATABASE'])) as dbh:
        with app.open_resource('setup.sql') as fh:
            dbh.cursor().executescript(fh.read())
        dbh.commit()

### Request bookend functions
@app.before_request
def before_request():
    g.dbh = connect_to_database(app.config['DATABASE'])

@app.teardown_request
def teardown_request(exception):
    g.dbh.commit()
    g.dbh.close()

### Web view request handlers
@app.route('/', methods=['GET'])
def view_all_groups():
    if not app.config.get('WEBVIEW'):
        abort(404)
    return render_template(
        'group_list.html', 
       	groups=db_get_newest_groups(g.dbh, 200)
    )

@app.route('/group/<hex_group_id>/', methods=['GET'])
def view_single_group(hex_group_id):
    if not app.config.get('WEBVIEW'):
        abort(404)
    group_id = UUID(hex=hex_group_id)

    # Produce random data for the input form
    names = None
    with open('/regroup_server/test_data/names.txt') as fh:
        names = fh.readlines()

    # Wash U campus (roughly)
    min_lat = 38.646014
    lat_range = .003049
    min_long = -90.314929
    long_range = .013704

    return render_template(
        'single_group.html',
        group=db_get_single_group_data(g.dbh, group_id),
        members=db_get_group_members_data(g.dbh, group_id),
        example = {
            'name': choice(names),
            'phone': '3145555555',
            'latitude': min_lat + uniform(0, lat_range),
            'longitude': min_long + uniform(0, long_range),
        }
    )

@app.route('/group/<hex_group_id>/add', methods=['POST'])
def view_add_single_member(hex_group_id):
    if not app.config.get('WEBVIEW'):
        abort(404)
    group_id = UUID(hex=hex_group_id)
    if request.form.get('name'):
        member_id = db_create_single_member(
            g.dbh,
            group_id,
            'WEBVIEW',
            request.form['name'],
            request.form['phone'],
            request.form['latitude'],
            request.form['longitude'],
            request.form['error']
        )
    else:
        g.alert = 'You must provide a member name.'
    return redirect(url_for('view_single_group', hex_group_id=hex_group_id))

### JSON Request handlers
@app.route('/api/v0/groups/', methods=['POST'])
def groups():
    group_id = db_create_single_group(
        g.dbh,
        request.json['name'],
        request.json['expiry'],
    )

    return jsonify(
        href=url_for('single_group', hex_group_id=group_id.hex),
        group_id=group_id.hex
    )

@app.route('/api/v0/groups/<hex_group_id>', methods=['GET', 'PUT'])
def single_group(hex_group_id):
    group_id = UUID(hex=hex_group_id)
    if request.method == 'GET':
        result = db_get_single_group_data(g.dbh, group_id)
        return jsonify(
            href=url_for(
                'single_group',
                hex_group_id=hex_group_id
            ),
            members = {'href': url_for('members', hex_group_id=hex_group_id)}, 
            name=result['name'],
            expiry=result['expiry'],
        )
    elif request.method == 'PUT':
        db_extend_single_group_expiry(
            g.dbh, 
            group_id, 
            request.json['expiry']
        )

@app.route('/api/v0/groups/<hex_group_id>/members/', methods=['GET', 'POST'])
def members(hex_group_id):
    group_id = UUID(hex=hex_group_id)
    if request.method == 'GET':
        result = db_get_group_members_data(g.dbh, group_id)
        retval = []
        for member in result:
            retval.append({
                'href': url_for(
                    'single_member',
                    hex_group_id=hex_group_id,
                    hex_member_id=member['member_id'].hex
                ),
                'full_name': member['full_name'],
                'phone': member['phone'],
                'latitude': member['latitude'],
                'longitude': member['longitude'],
                'error_radius': member['error_radius'],
            })

        return jsonify(
            href=url_for('members', hex_group_id=hex_group_id),
            members=retval
        )
    elif request.method == 'POST':
        member_id = db_create_single_member(
            g.dbh,
            group_id,
            request.json['client_id'],
            request.json['full_name'],
            request.json['phone'],
            request.json['latitude'],
            request.json['longitude'],
            request.json['error_radius']
        )
        return jsonify(
            href=url_for(
                'single_member', 
                hex_group_id=hex_group_id,
                hex_member_id=member_id.hex,
            )
        )

@app.route('/api/v0/groups/<hex_group_id>/members/<hex_member_id>', methods=['GET', 'PUT', 'DELETE'])
def single_member(hex_group_id, hex_member_id):
    group_id = UUID(hex=hex_group_id)
    member_id = UUID(hex=hex_member_id)
    if request.method == 'GET':
        result = db_get_single_member_data(
            g.dbh,
            group_id,
            member_id
        )
        return jsonify(
            href=url_for(
                'single_member', 
                hex_group_id=hex_group_id,
                hex_member_id=hex_member_id
            ),
            full_name=result['full_name'],
            phone=result['phone'],
            latitude=result['latitude'],
            longitude=result['longitude'],
            error_radius=result['error_radius']
        )
    elif request.method == 'PUT':
        db_update_single_member(
            g.dbh,

            group_id,
            member_id,
            request.json['client_id'],

            request.json.get('full_name'),
            request.json.get('phone'),
            request.json.get('latitude'),
            request.json.get('longitude'),
            request.json.get('error_radius')
        )
        return jsonify(
            href=url_for(
                'single_member', 
                hex_group_id=hex_group_id,
                hex_member_id=hex_member_id
            )
        )
    elif request.method == 'DELETE':
        db_delete_single_member(
            g.dbh,
            group_id,
            member_id,
            request.json['client_id']
        )
        return ''

### Start the application
if __name__ == '__main__':
    app.run()

### TODO
#   Validation of parameters
#   Error handling

