import sqlite3
from contextlib import closing
from uuid import UUID, uuid4 

### TODO
#   Validation of paramaters
#   Error handling

### Database setup
def connect_to_database(database):
    return sqlite3.connect(database)


### Data retrieval fuctions
def db_get_newest_groups(dbh, count):
    cursor = dbh.execute(
        "SELECT id, name, expiry FROM Groups ORDER BY expiry DESC LIMIT ?",
        (count,)
    )
    results = cursor.fetchall()
    return [
        {
            'group_id':     UUID(bytes=result[0]),
            'name':         result[1],
            'expiry':       result[2],
        } for result in results
    ]

def db_get_single_group_data(dbh, group_id):
    cursor = dbh.execute(
        "SELECT name, expiry FROM Groups WHERE id = ? LIMIT 1",
        (buffer(group_id.bytes),)
    )
    results = cursor.fetchone()
    return {
        'group_id':     group_id,
        'name':         results[0],
        'expiry':       results[1],
    }
    

def db_get_group_members_data(dbh, group_id):
    cursor = dbh.execute(
        '''SELECT id, full_name, phone, latitude, longitude, error_radius
                FROM Members WHERE group_id = ?''',
        (buffer(group_id.bytes), )
    )
    results = cursor.fetchall()
    return [
        {
            'group_id':     group_id,
            'member_id':    UUID(bytes=result[0]),
            'full_name':    result[1],
            'phone':        result[2],
            'latitude':    result[3],
            'longitude':    result[4],
            'error_radius': result[5],
        } for result in results
    ]

def db_get_single_member_data(dbh, group_id, member_id):
    cursor = dbh.execute(
        '''SELECT full_name, phone, latitude, longitude, error_radius
                FROM Members WHERE group_id = ? AND id = ? LIMIT 1''',
        (buffer(group_id.bytes), buffer(member_id.bytes))
    )
    result = cursor.fetchone()
    return {
        'group_id':     group_id,
        'member_id':    member_id,
        'full_name':    result[0],
        'phone':        result[1],
        'latitude':    result[2],
        'longitude':    result[3],
        'error_radius': result[4],
    }

### Data modification functions
def db_create_single_group(dbh, name, expiry):
    """ 
    Creates a group with a name and ISO timestamp expiration.
    Returns the new group's UUID object.
    """
    group_id = uuid4()
    cursor = dbh.execute(
        '''INSERT INTO Groups (id, name, expiry) 
            VALUES (?, ?, ?)''',
        (buffer(group_id.bytes), name, expiry)
    )
    return group_id

def db_extend_single_group_expiry(dbh, group_id, expiry):
    """
    Sets the given group's expiry to the given value if the given value
    is later than the existing expiry.
    """
    cursor = dbh.execute(
        '''UPDATE Groups
            SET expiry = :exp
            WHERE id = :gid AND expiry < :exp''',
        {
            'gid':  buffer(group_id.bytes),
            'exp':  expiry,
        }
    )

def db_create_single_member(
    dbh, 
    group_id, client_id, full_name, phone, latitude, longitude, error_radius):
    """
    Creates a new member in the given group, whose ID is represented by a UUID
    object.
    Returns the new member's UUID object.
    """
    member_id = uuid4()
    cursor = dbh.execute(
        '''INSERT INTO Members (
            group_id, id, 
            client_id, full_name, phone, 
            latitude, longitude, error_radius
        ) VALUES (
            ?, ?,
            ?, ?, ?,
            ?, ?, ?
        )''',
        (
            buffer(group_id.bytes), buffer(member_id.bytes),
            client_id, full_name, phone,
            latitude, longitude, error_radius,
        )
    )

    return member_id

def db_update_single_member(
    dbh,
    group_id, member_id, client_id, 
    full_name, phone, latitude, longitude, error_radius):
    """
    Updates a member's info when the group_id, member_id, and client_id match.
    The attribute fields are only updated when the new value is not null.
    """
    cursor = dbh.execute(
        '''UPDATE Members
            SET
                full_name = COALESCE(:fn, full_name),
                phone = COALESCE(:ph, phone),
                latitude = COALESCE(:lat, latitude),
                longitude = COALESCE(:long, longitude),
                error_radius = COALESCE(:err, error_radius)
            WHERE id = :mid AND group_id = :gid AND client_id = :cid''',
        {
            'gid':  buffer(group_id.bytes),
            'mid':  buffer(member_id.bytes),
            'cid':  client_id,
            'fn':   full_name,
            'ph':   phone,
            'lat':  latitude,
            'long': longitude,
            'err':  error_radius,
        }
    )

def db_delete_single_member(dbh, group_id, member_id, client_id):
    """
    Deletes a member when the group_id, member_id, and client_id match.
    """
    cursor = dbh.execute(
        '''DELETE FROM Members
            WHERE id = :mid AND group_id = :gid AND client_id = :cid''',
        {
            'gid':  buffer(group_id.bytes),
            'mid':  buffer(member_id.bytes),
            'cid':  client_id,
        }
    )
